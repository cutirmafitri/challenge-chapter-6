
// connect to DB
const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres', // your db user
  host: 'localhost',
  database: 'challenge6', // your db name
  password: '1997iloveAllah', // your db password
  port: 5432,
})
const getUser_game = (request, response) => {
  pool.query('SELECT * FROM user_game ORDER BY id ASC', (error, results) => {
    if (error) {
      response.status(500).json(error)
    }
    response.status(200).json(results.rows)
  })
}

const getUser_gameById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM user_game WHERE id =$1', [id], (error, results) => { // query: get player by id
    if (error) {
        response.status(500).json(error)
    }
    response.status(200).json(results.rows)
  })
}

const createUser_game = (request, response) => {
  const { name, email } = request.body

  pool.query('INSERT INTO user_game (name, email) VALUES ($1, $2) RETURNING *', [name, email], (error, results) => { // set values for name and email
    if (error) {
        response.status(500).json(error)
    } else if (!Array.isArray(results.row) || results.rows.length < 1) { // if results has no rows or there is no result
    	response.status(500).json(error)
    }
    response.status(201).send(`User_game added with ID: ${results.rows[0].id}`)
  })
}

const updateUser_game = (request, response) => {
  const id = parseInt(request.params.id)
  const { name, email } = request.body // get data from request body

  pool.query(
    'UPDATE user_game SET name = $1, email = $2 WHERE id = $3 RETURNING *',
    [name, email, id], // entries for query
    (error, results) => {
      if (error) {
        response.status(500).send('Internal Server Eror'); // send response with status 500 or internal server error
      }
      if (typeof results.rows == 'undefined') {
      	response.status(404).send(`Resource not found`);
      } else if (Array.isArray(results.rows) && results.rows.length < 1) {
      	response.status(404).send('User Not Found'); // send 404 status with information 'Player not found'
      } else {
  	 	  response.status(200).send(`User modified with ID: ${results.rows[0].id}`)
      }
    }
  )
}

const deleteUser_game = (request, response) => {
  const id = parseInt(request.params.id) // get data from request parameter (id)

  pool.query('DELETE FROM user_game WHERE id = $1', [id], (error, results) => {
    if (error) {
      response.status(500).json(error); // send json with status 500
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}



module.exports = {
  getUser_game,
  getUser_gameById,
  createUser_game,
  updateUser_game,
  deleteUser_game,
}
