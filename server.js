const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./queries') // import queries file
const port = 8000

// membuat server bisa membaca request body
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ info: 'This is Dashboard' })
})

// routing
app.get('/user_game', db.getUser_game) // list all User_game
app.get('/user_game/:id', db.getUser_gameById) // get single User_game based on id
app.post('/user_game', db.createUser_game) // create new User_game
app.put('/user_game/:id', db.updateUser_game) // update existing User_game data
app.delete('/user_game/:id', db.deleteUser_game) // delete User_game data

app.get('/login', (req, res) => {
    res.render('login')
})

app.post('/login', (req, res) => {
    if (req.body.username === 'superadmin' && req.body.password === 'qwertyadmin') {
        res.render('home')
    } else {
        res.status(401).send({'message': 'unauthorized'})
    }
})

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})
