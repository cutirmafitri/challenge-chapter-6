--bikin tabel via pgadmin
-- buat data via mockaroo

CREATE TABLE user_game (
  id INT NOT NULL PRIMARY KEY,
  username VARCHAR(30),
  password VARCHAR(50) NOT NULL 
);

CREATE TABLE user_game_biodata (
  email VARCHAR(255) UNIQUE NOT NULL,
  created_on TIMESTAMP NOT NULL,
  age INT DEFAULT 0,
  country VARCHAR(255) DEFAULT ''
);

CREATE TABLE user_game_history (
  grant_date TIMESTAMP,
  last_login TIMESTAMP,
  session_date DATE DEFAULT now(),
  score INT NOT NULL  
);

INSERT INTO user_game (id, username, password)
VALUES
    (1, 'sakti', 'qwerty'),
    (2, 'zain', 'lalilulelo'),
    (3, 'bella', 'hbdaja');

INSERT INTO user_game_biodata (email, age, created_on, country)
VALUES
    ('sakti@gmail.com', 23, CURRENT_TIMESTAMP, 'SAUDI ARABIA'),
    ('zain@yahoo.com', 23, CURRENT_TIMESTAMP, 'JAPAN'),
    ('bella@indonesia.com', 26, CURRENT_TIMESTAMP, 'INDONESIA');

INSERT INTO user_game_history (grant_date, last_login, session_date, score)
VALUES
    (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_DATE, 102),
    (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_DATE, 34),
    (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, CURRENT_DATE, 78);